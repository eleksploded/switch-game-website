
export async function getTitleDB(onComplete) {
    function games(data) {
        var games = []
        for (const [key, value] of Object.entries(data)) {
            games.push(value)
        }
        return games
    }

    fetch("https://raw.githubusercontent.com/blawar/titledb/master/US.en.json")
        .then(res => res.json())
        .then(data => onComplete(games(data)))
}

