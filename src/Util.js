Number.prototype.compareTo = function (num) {
    if (typeof num != "number") return false; // if number is not of type number return false

    if (num < this) return 1;
    else if (num > this) return -1;
    else return 0;
};

Date.prototype.compareTo = function (date) {
    if (date < this) return 1;
    else if (date > this) return -1;
    else return 0;
}

export function humanFileSize(bytes, si = false, dp = 1) {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }

    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
        bytes /= thresh;
        ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


    return bytes.toFixed(dp) + ' ' + units[u];
}

export function toDate(str) {
    if (str === null || str === "") return new Date(0)
    var y = String(str).substring(0, 4)
    var m = String(str).substring(4, 6)
    var d = String(str).substring(6, 8)

    return new Date(m + "/" + d + "/" + y)
}