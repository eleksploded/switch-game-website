import './App.css';
import { useState, useEffect } from 'react';
import { getTitleDB } from './TitleDB.js'
import GameList from './GameList';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import { GameViewWrap } from './GameView';
import { Dropdown } from "./Dropdown"

function MainPage() {
  const [page, setPage] = useState(0)
  const [perPage, setPerPage] = useState(25)
  const [sortBy, setSortBy] = useState("name")
  const [revSort, setRevSort] = useState(false)
  const [games, setGames] = useState( [] )
  const [searchType, setSearchType] = useState("name")
  const [search, setSearch] = useState("")
  
  useEffect(() => {
    getTitleDB(titles => setGames(titles))
  }, []);

  function getView() {
    if (games === null || games.length === 0) {
      return (
        <div className='viewLoading'>
          <h2>Loading...</h2>
        </div>
      )
    } else {
      return (
        <div className='view'>
          <GameList games={games} page={page} perPage={perPage} sortBy={sortBy} setSortBy={setSortBy} 
            revSort={revSort} setRevSort={setRevSort} searchType={searchType} search={search}
          />
        </div>
      )
    }
  }

  function controller() {
    var max = (games !== null && games.length !== 0) ? Math.floor(games.length / perPage) : 0;
    return (
      <div className='controller'>
        <button onClick={() => setPage(Math.max(0, page - 1))}>{"<"}</button>
        <h3>Page: {page + 1} / {max}</h3>
        <button onClick={() => setPage(Math.min(page + 1, max))}>{">"}</button>
        <div className='controllerSpacer' />
        <div className='controllerperpage'>
          <h3>Per Page:</h3>
          <input
            type="text"
            value={perPage}
            onChange={(e) => {
              if(parseInt(e.target.value)) {
                setPerPage(parseInt(e.target.value))
              } else {
                setPerPage(0)
              }
            }}
          />
        </div>
        <div className='controllerperpage'>
          <Dropdown
            trigger={<button className='dropdownButton'>Search  by {searchType}</button>}
            menu={[
              <button onClick={() => setSearchType("name")}>Name</button>,
              <button onClick={() => setSearchType("id")}>ID</button>,
              <button onClick={() => setSearchType("before")}>Released Before</button>,
              <button onClick={() => setSearchType("after")}>Released After</button>
            ]}
          />
          <input
            type="text"
            value={search}
            onChange={(e) => {
              setSearch(e.target.value)
              setPage(0)
            }}
          />
        </div>
      </div>
    )
  }
  
  return (
    <div className='App'>
      {controller()}
      <div className='viewBox'> {getView()} </div>
    </div>
  )
}

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="*" element={<GameViewWrap />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App;
