import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { humanFileSize, toDate } from './Util';


const sorters = {
    "name": (a, b) => String(a["name"]).localeCompare(String(b["name"])),
    "playerCount": (a, b) => (a["numberOfPlayers"] != null ? a["numberOfPlayers"] : 0).compareTo((b["numberOfPlayers"] != null ? b["numberOfPlayers"] : 0)),
    "ID": (a, b) => String(a["id"]).localeCompare(String(b["id"])),
    "size": (a, b) => a["size"].compareTo(b["size"]),
    "release": (a, b) => toDate(a["releaseDate"]).compareTo(toDate(b["releaseDate"]))
}

const filters = {
    "name": (e, search) => String(e["name"]).toLowerCase().includes(String(search).toLowerCase()),
    "id": (e, search) => String(e["id"]).toLowerCase().includes(String(search).toLowerCase()),
    "after": (e, search) => toDate(e["releaseDate"]).getFullYear !== 1969 ? toDate(e["releaseDate"]) >= new Date(search): false,
    "before": (e, search) => toDate(e["releaseDate"]).getFullYear !== 1969 ? toDate(e["releaseDate"]) <= new Date(search) : false
}

function GameList({ games, page, perPage, sortBy, setSortBy, revSort, setRevSort, searchType, search}) {
    function getGames() {
        games.sort(sorters[sortBy]);
        if(revSort) games.reverse()
        var gArray = (search !== null && search !== "") ? games.filter((e) => filters[searchType](e, search)) : games

        var start = Math.min(page * perPage, gArray.length - 1);
        var end = Math.min((page + 1) * perPage, gArray.length)
        const gRet = []
        for(var i = start; i < end; i++) {
            gRet.push(
                <Game game={gArray[i]} key={i}/>
            )
        }
        return gRet;
    }

    function getArrow(type) {
        if(sortBy === type) {
            return revSort ? "⌃"  : "⌄"
        } else return ""
    }

    return (
        <div>
            <table>
                <tr>
                    <th className="headerButton">Icon</th>
                    <th><button className="headerButton" onClick={() => setSort("ID", sortBy, setSortBy, revSort, setRevSort)}>Title ID {getArrow("ID")}</button></th>
                    <th><button className="headerButton" onClick={() => setSort("name", sortBy, setSortBy, revSort, setRevSort)}>Name {getArrow("name")}</button></th>
                    <th><button className="headerButton" onClick={() => setSort("playerCount", sortBy, setSortBy, revSort, setRevSort)}>Player Count {getArrow("playerCount")}</button></th>
                    <th><button className="headerButton" onClick={() => setSort("release", sortBy, setSortBy, revSort, setRevSort)}>Release Date {getArrow("release")}</button></th>
                    <th><button className="headerButton" onClick={() => setSort("size", sortBy, setSortBy, revSort, setRevSort)}>Size {getArrow("size")}</button></th>
                </tr>
                {getGames()}
            </table>
        </div>
        
    )
}

function setSort(newType, oldType, setSortBy, revSort, setRevSort) {
    if(oldType === newType) {
        setRevSort(!revSort);
    } else {
        setSortBy(newType);
        setRevSort(false)
    }
}

function Game({game}) {
    var nav = useNavigate()
    if(game === undefined) {
        return <tr>
            <td>N/A</td>
            <td>N/A</td>
            <td>Undefined</td>
            <td>-</td>
            <td>-</td>
        </tr>
    }

    var date = toDate(game["releaseDate"])
    var dString = date.getFullYear() !== 1969 ? date.toLocaleDateString() : ""

    return (
        <tr onClick={() => nav(game["id"])}>
            <td><img src={game["iconUrl"] != null ? game["iconUrl"] : ""} width="16" height="16" /></td>
            <td>{game["id"]}</td>
            <td>{game["name"]}</td>
            <td>{game["numberOfPlayers"]}</td>
            <td>{dString}</td>
            <td>{humanFileSize(game["size"])}</td>
        </tr>
    )
}

export default GameList;