import { useLocation, useNavigate } from "react-router-dom";
import { useState, useEffect } from 'react';
import { getTitleDB } from './TitleDB.js'
import { humanFileSize, toDate} from './Util';
import "./GameView.css"

export function GameViewWrap() {
    const [game, setGame] = useState(-1)
    const route = useLocation().pathname.replace("/", "");

    useEffect(() => {
        getTitleDB(titles => {
            setGame(titles.find(e => e["id"] === route))
        })
    }, [route]);

    if(game === -1) return <h1 className="loading">Loading...</h1>

    if(game) {
        return <GameView game={game} />
    } else {
        return <UnknownGame path={route}/>
    }
}

function UnknownGame({path}) {
    var nav = useNavigate();
    return (
        <div className="unknown" onClick={() => nav("/")}>
            <h1>Unknown game: {path}</h1>
            <h3>Click anywhere to go back</h3>
        </div>
    )
}

function GameView({game}) {
    const nav = useNavigate();

    function getImages() {
        var imgs = []
        var ss = game["screenshots"];
        if (game["screenshots"]) {
            for (let i = 0; i < ss.length; i++) {
                imgs.push(<img className="screenshot" src={ss[i]} key={i} alt="game screenshot"/>)
            }
        }
        return imgs;
    }

    return (
        <div className="gameview">
            <div className="toparea">
                <div className="banner"><img className="bannerImage" src={game["bannerUrl"] ? game["bannerUrl"] : ""} alt="game banner"/></div>
                <div className="info">
                    <div className="topInfo">
                        <div className="titleid"><h4 className="text">ID: {game["id"]}</h4></div>
                    </div>
                    <div className="name"><h1 className="text gamename">{game["name"]}</h1></div>
                    <div className="descr"><p className="descText">{game["description"]}</p></div>
                </div>
            </div>
            <div className="bottomarea">
                <div className="advinfo">
                    <p>Number of Players: {game["numberOfPlayers"] ? game["numberOfPlayers"] : "-"}</p>
                    <p>Release Date: {toDate(game["releaseDate"]).getFullYear() !== 1969 ? toDate(game["releaseDate"]).toLocaleDateString() : "Unknown"}</p>
                </div>
                <div className="screenshots">
                    {getImages()}
                </div>
                <div className="advinfo">
                    <p>Region: {game["region"] ? game["region"] : "N/A"}</p>
                    <p>NSU ID: {game["nsuId"]}</p>
                    <p>Publisher: {game["publisher"]}</p>
                    <p>Size: {humanFileSize(game["size"])}</p>
                </div>
                <div className="advinfo" onClick={() => nav("/")}>
                    <p>Click to go back</p>
                </div>
            </div>
        </div>
    )
}